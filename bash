#!/bin/bash

                      
echo '  _____ _____ _____ _____  '
echo ' | __  |  _  |   __|  |  | '
echo ' | __ -|     |__   |     | '
echo ' |_____|__|__|_____|__|__| '
echo '                           '

echo '                                     '
echo ' _____ _____ _____ _____ _____ _____ '
echo '|     |     |   | |   __|     |   __|'
echo '|   --|  |  | | | |   __|-   -|  |  |'
echo '|_____|_____|_|___|__|  |_____|_____|'
echo '                                     '

FILE=$HOME/.config/
if [ ! -d "$FILE" ]; then
    mkdir -p $FILE
else
    echo "~/.config exist"
fi

FILE=$HOME/.config/bash
if [ ! -d "$FILE" ]; then
    mkdir -p $FILE
else
    echo "~/.config/bash exist"
fi

cp -f config/bash/bashrc $HOME/.config/bash/bashrc

mv $HOME/.bashrc $HOME/.bashrc_old
sudo mv /root/.bashrc /root/.bashrc_old

ln -s $HOME/.config/bash/bashrc $HOME/.bashrc
sudo ln -s $HOME/.config/bash/bashrc /root/.bashrc
