if [ -t 1 ]; then
  RB_RED=$(printf '\033[38;5;196m')
  RB_ORANGE=$(printf '\033[38;5;202m')
  RB_YELLOW=$(printf '\033[38;5;226m')
  RB_GREEN=$(printf '\033[38;5;082m')
  RB_BLUE=$(printf '\033[38;5;021m')
  RB_INDIGO=$(printf '\033[38;5;093m')
  RB_VIOLET=$(printf '\033[38;5;163m')

  RED=$(printf '\033[31m')
  GREEN=$(printf '\033[32m')
  YELLOW=$(printf '\033[33m')
  BLUE=$(printf '\033[34m')
  BOLD=$(printf '\033[1m')
  UNDER=$(printf '\033[4m')
  RESET=$(printf '\033[m')
else
  RB_RED=""
  RB_ORANGE=""
  RB_YELLOW=""
  RB_GREEN=""
  RB_BLUE=""
  RB_INDIGO=""
  RB_VIOLET=""

  RED=""
  GREEN=""
  YELLOW=""
  BLUE=""
  UNDER=""
  BOLD=""
  RESET=""
fi

git config core.eol lf
git config core.autocrlf false
# zeroPaddedFilemode fsck errors (#4963)
git config fsck.zeroPaddedFilemode ignore
git config fetch.fsck.zeroPaddedFilemode ignore
git config receive.fsck.zeroPaddedFilemode ignore
# autostash on rebase (#7172)
resetAutoStash=$(git config --bool rebase.autoStash 2>&1)
git config rebase.autoStash true

remote=$(git remote -v | awk  '/https:\/\/gitlab\.com\/ubikOne\/vim-config\.git/{ print $1; exit }')
if [ -n "$remote" ]; then
  git remote set-url "$remote" "https://gitlab.com/ubikOne/vim-config.git"
fi

printf "${BLUE}%s${RESET}\n" "Updating vim config"
if git pull --rebase --stat origin master
then
 
echo '  _   __       __      ____ ___    '
echo ' | | / / /    / / /   / __ `__ \ \ '
echo ' | |/ / /    / / /   / / / / / / / '
echo ' |___/ /    /_/ /   /_/ /_/ /_/ /  '
echo '  ____/     ___/    __/ __/ ___/   '
echo '                                   '

  printf "${BLUE}%s\n" "Hooray! vim config has been updated and/or is at the current version."
  cp -f config/vim/vimrc ~/.config/vim/vimrc
  vim -E -s -u "~/.vimrc" +PluginInstall +qall
else
  status=$?
  printf "${RED}%s${RESET}\n" 'There was an error updating. Try again later?'
fi

# Unset git-config values set just for the upgrade
case "$resetAutoStash" in
  "") git config --unset rebase.autoStash ;;
  *) git config rebase.autoStash "$resetAutoStash" ;;
esac

# Exit with `1` if the update failed
exit $status

