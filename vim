#!/bin/bash
                   
echo ' _____ _____ _____ '
echo '|  |  |     |     |'
echo '|  |  |-   -| | | |'
echo ' \___/|_____|_|_|_|'
echo '                   '

echo '                                     '
echo ' _____ _____ _____ _____ _____ _____ '
echo '|     |     |   | |   __|     |   __|'
echo '|   --|  |  | | | |   __|-   -|  |  |'
echo '|_____|_____|_|___|__|  |_____|_____|'
echo '                                     '

FILE=~/.config/
if [ ! -d "$FILE" ]; then
    mkdir -p $FILE
else
    echo "~/.config exist"
fi

FILE=~/.config/vim/
if [ -d "$FILE" ]; then
    sudo rm -R $FILE
fi

mkdir -p ~/.config/vim

FILE=~/.vimrc
if [ -f "$FILE" ]; then
    mv -f ~/.vimrc ~/.vimrc.old
fi

cp -f config/vim/vimrc ~/.config/vim/vimrc

ln -s ~/.config/vim/vimrc ~/.vimrc
sudo ln -s ~/.config/vim/vimrc /root/.vimrc
sudo ln -s ~/.vim/ /root/.vim

PIP_JEDI=$(pip3 freeze | sed '{s/=/ /}' | awk '{print $1}' | grep jedi)
PIP_PYNVIM=$(pip3 freeze | sed '{s/=/ /}' | awk '{print $1}' | grep pynvim)

if [[ $PIP_JEDI == 'jedi' ]]; then
   echo "pip jedi is installed"
else
   sudo pip3 install jedi
fi

if [[ $PIP_PYNVIM == 'pynvim' ]]; then
   echo "pip pynvim is installed"
else
   sudo pip3 install pynvim 
fi

FILE=~/.vim/bundle/Vundle.vim
if [ -d "$FILE" ]; then
    sudo rm -R $FILE
fi

git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim

vim -E -s -u '~/.vimrc' +PluginInstall +qall
