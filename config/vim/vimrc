"  ██╗   ██╗██╗███╗   ███╗                        
"  ██║   ██║██║████╗ ████║                        
"  ██║   ██║██║██╔████╔██║                        
"  ╚██╗ ██╔╝██║██║╚██╔╝██║                        
"   ╚████╔╝ ██║██║ ╚═╝ ██║                        
"    ╚═══╝  ╚═╝╚═╝     ╚═╝                        
"                                                 
"   ██████╗ ██████╗ ███╗   ██╗███████╗██╗ ██████╗ 
"  ██╔════╝██╔═══██╗████╗  ██║██╔════╝██║██╔════╝ 
"  ██║     ██║   ██║██╔██╗ ██║█████╗  ██║██║  ███╗
"  ██║     ██║   ██║██║╚██╗██║██╔══╝  ██║██║   ██║
"  ╚██████╗╚██████╔╝██║ ╚████║██║     ██║╚██████╔╝
"   ╚═════╝ ╚═════╝ ╚═╝  ╚═══╝╚═╝     ╚═╝ ╚═════╝ 
"                                                 
"  ███████╗██╗██╗     ███████╗                    
"  ██╔════╝██║██║     ██╔════╝                    
"  █████╗  ██║██║     █████╗                      
"  ██╔══╝  ██║██║     ██╔══╝                      
"  ██║     ██║███████╗███████╗                    
"  ╚═╝     ╚═╝╚══════╝╚══════╝                    
"                                                 


" VIM Config File
" Author: Bartek 'ubikOne' Błażejewski
" version 1.0.0
" License GNU GPL ver. 3

" BASIC CONFIG -------------------------------------------------------- {{{

  set nocompatible
  filetype on
  filetype plugin on
  filetype indent on
  filetype plugin indent on

  syntax on

  set cursorline
  set cursorcolumn

  set number relativenumber
  set shiftwidth=2
  set tabstop=2
  set expandtab
  set nobackup
  set scrolloff=10
  set incsearch
  set ignorecase
  set smartcase
  set showcmd
  set showmode
  set showmatch
  set hlsearch
  set history=1000

  set nu rnu
  set autoindent
  set smartindent

  set wildmenu
  set wildmode=list:longest
  set wildignore=*.docx,*.jpg,*.png,*.gif,*.pdf,*.pyc,*.exe,*.flv,*.img,*.xlsx

  set encoding=utf-8
  set fileencoding=utf-8
  set fileencodings=utf-8

  cabbrev bterm bo term
  cabbrev vterm vert term

  set splitbelow
  set splitright

" }}}

" PLUGINS ------------------------------------------------------------- {{{

  set rtp+=~/.vim/bundle/Vundle.vim

  call vundle#begin()

    Plugin 'VundleVim/Vundle.vim'
    Plugin 'dense-analysis/ale'
    Plugin 'preservim/nerdtree'
    Plugin 'Xuyuanp/nerdtree-git-plugin'
    Plugin 'morhetz/gruvbox'
    Plugin 'vim-airline/vim-airline'
    Plugin 'vim-airline/vim-airline-themes'
    Plugin 'terryma/vim-multiple-cursors'
    Plugin 'altercation/vim-colors-solarized'
    Plugin 'tpope/vim-fugitive'
    Plugin 'airblade/vim-gitgutter'
    Plugin 'zivyangll/git-blame.vim'
    Plugin 'preservim/nerdcommenter'
    Plugin 'nathanaelkane/vim-indent-guides'
    Plugin 'tpope/vim-markdown' 
    Plugin 'sheerun/vim-polyglot'
    Plugin 'Shougo/deoplete.nvim'
    Plugin 'roxma/nvim-yarp'
    Plugin 'roxma/vim-hug-neovim-rpc'
    Plugin 'ctrlpvim/ctrlp.vim'
    Plugin 'qpkorr/vim-bufkill'
    Plugin 'mhinz/vim-startify'

  call vundle#end()

" }}}

" THEMES -------------------------------------------------------------- {{{

  let g:airline#extensions#tabline#enabled = 1
  let g:airline#extensions#tabline#left_sep = ' '
  let g:airline#extensions#tabline#left_alt_sep = '|'
  let g:airline#extensions#tabline#formatter = 'default'

  let g:airline_theme='wombat'

  set background=dark
  let g:solarized_termcolors=256
  colorscheme gruvbox

" }}}

" STARTIFY ------------------------------------------------------------ {{{

let g:startify_session_dir = '~/.config/nvim/session'

let g:startify_lists = [
  \ { 'type': 'files',     'header': ['   Files']            },
  \ { 'type': 'dir',       'header': ['   Current Directory '. getcwd()] },
  \ { 'type': 'sessions',  'header': ['   Sessions']       },
  \ { 'type': 'bookmarks', 'header': ['   Bookmarks']      },
  \ ]

let g:startify_bookmarks = [
  \ { 'u': '~/Projekty/home/ubikcode/' },
  \ ]
 
let g:startify_custom_header = [
  \' ██╗   ██╗ ██████╗ ██╗   ██╗██╗███╗   ███╗',
  \' ██║   ██║██╔═══██╗██║   ██║██║████╗ ████║',
  \' ██║   ██║██║   ██║██║   ██║██║██╔████╔██║',
  \' ██║   ██║██║   ██║╚██╗ ██╔╝██║██║╚██╔╝██║',
  \' ╚██████╔╝╚██████╔╝ ╚████╔╝ ██║██║ ╚═╝ ██║',
  \'  ╚═════╝  ╚═════╝   ╚═══╝  ╚═╝╚═╝     ╚═╝',
  \]

" }}}

" NERDCOMMENTER ------------------------------------------------------- {{{

  let g:NERDCreateDefaultMappings = 1
  let g:NERDSpaceDelims = 1
  let g:NERDCompactSexyComs = 1
  let g:NERDDefaultAlign = 'left'
  let g:NERDAltDelims_java = 1
  let g:NERDCustomDelimiters = { 'c': { 'left': '/**','right': '*/' } }
  let g:NERDCommentEmptyLines = 1
  let g:NERDTrimTrailingWhitespace = 1
  let g:NERDToggleCheckAllLines = 1

" }}}

" GITGUTTER ----------------------------------------------------------- {{{

  let g:gitgutter_sign_added = '+'
  let g:gitgutter_sign_modified = '>'
  let g:gitgutter_sign_removed = '-'
  let g:gitgutter_sign_removed_first_line = '^'
  let g:gitgutter_sign_modified_removed = '<'

  let g:gitgutter_override_sign_column_highlight = 1
  highlight SignColumn guibg=bg
  highlight SignColumn ctermbg=bg

" }}}

" MAPPINGS ------------------------------------------------------------ {{{

  let mapleader = "\<space>"
  nnoremap <leader>\ ``
  nnoremap o o<esc>
  nnoremap O O<esc>
  nnoremap <Leader>tb :bo term<CR>
  nnoremap <Leader>tv :vert term<CR>
  nnoremap <Leader>z ZZ
  nnoremap <Leader>q :q!<CR>
  nnoremap <Leader>w :w<CR>
  nnoremap <Leader><space> :

  nnoremap <Leader>/ca zm
  nnoremap <Leader>/co zc
  nnoremap <Leader>/oa zr
  nnoremap <Leader>/oo zo

  nnoremap <C-t>     :tabnew<CR>
  nnoremap <leader>tn :tabnew<CR>
  inoremap <C-t>     <Esc>:tabnew<CR>
  nnoremap <C-j> gT
  nnoremap <C-k> gt

  au TabLeave * let g:lasttab = tabpagenr()
  nnoremap <silent> <c-l> :exe "tabn ".g:lasttab<cr>
  vnoremap <silent> <c-l> :exe "tabn ".g:lasttab<cr>

  map <C-o> :NERDTreeToggle<CR>
  nnoremap <C-f> :NERDTreeFind<CR>

  let g:multi_cursor_start_word_key      = '<C-n>'
  let g:multi_cursor_select_all_word_key = '<A-n>'
  let g:multi_cursor_start_key           = 'g<C-n>'
  let g:multi_cursor_select_all_key      = 'g<A-n>'
  let g:multi_cursor_next_key            = '<C-n>'
  let g:multi_cursor_prev_key            = '<C-p>'
  let g:multi_cursor_skip_key            = '<C-x>'
  let g:multi_cursor_quit_key            = '<Esc>'

  nmap ]h <Plug>(GitGutterNextHunk)
  nmap [h <Plug>(GitGutterPrevHunk)

  nnoremap <Leader>ga :Git add .<CR>
  nnoremap <Leader>gc :Git commit<CR>
  nnoremap <Leader>gsh :Git push<CR>
  nnoremap <Leader>gll :Git pull<CR>
  nnoremap <Leader>gs :Git status<CR>
  nnoremap <Leader>gb :Git blame<CR>
  nnoremap <Leader>gdd :Gvdiff<CR>
  nnoremap <Leader>gd :Git diff<CR>
  nnoremap <Leader>gr :Git remove<CR>
  nnoremap <Leader>gss :<C-u>call gitblame#echo()<CR>

  noremap <silent> <leader>c} V}:call NERDComment('x', 'toggle')<CR>
  nnoremap <silent> <leader>c{ V{:call NERDComment('x', 'toggle')<CR>

  nnoremap <leader>bn :bn<CR>
  nnoremap <leader>bf :BF<CR> 
  nnoremap <leader>bp :BB<CR> 
  nnoremap <leader>bd :BD<CR> 
  nnoremap <leader>bl :buffers!<CR>

  nnoremap <leader>sn :ne<CR>

  nnoremap <leader>sh  :sp<CR>
  nnoremap <leader>sv  :vsp<CR>
  nnoremap <leader>sw <C-W><C-K>
  nnoremap <leader>ss <C-W><C-J>
  nnoremap <leader>sa <C-W><C-H>
  nnoremap <leader>sd <C-W><C-L>

" }}}

" GIT ----------------------------------------------------------------- {{{

  let g:NERDTreeGitStatusIndicatorMapCustom= {
    \ "Modified"  : "✹",
    \ "Staged"    : "✚",
    \ "Untracked" : "✭",
    \ "Renamed"   : "➜",
    \ "Unmerged"  : "═",
    \ "Deleted"   : "✖",
    \ "Dirty"     : "✗",
    \ "Clean"     : "✔︎",
    \ 'Ignored'   : '☒',
    \ "Unknown"   : "?"
  \ }

  let g:NERDTreeGitStatusShowIgnored = 1
  let NERDTreeShowHidden = 1

" }}}

" OTHER --------------------------------------------------------------- {{{

  let g:indent_guides_enable_on_vim_startup = 1
  let g:indent_guides_guide_size = 1

  let g:markdown_fenced_languages = ['html', 'python', 'bash=sh']
  let g:markdown_syntax_conceal = 0
  let g:markdown_minlines = 100

  let g:deoplete#enable_at_startup = 1

" }}}

" CTRLP --------------------------------------------------------------- {{{

  set wildignore+=*/tmp/*,*.so,*.swp,*.zip     " MacOSX/Linux

  let g:ctrlp_custom_ignore = '\v[\/]\.(git|hg|svn)$'
  let g:ctrlp_custom_ignore = {
    \ 'dir':  '\v[\/]\.(git|hg|svn)$',
    \ 'file': '\v\.(exe|so|dll)$',
    \ 'link': 'some_bad_symbolic_links',
    \ }

" }}}

" VIMSCRIPT ----------------------------------------------------------- {{{

  augroup filetype_vim
    autocmd!
    autocmd FileType vim setlocal foldmethod=marker
  augroup END

  augroup cursor_off
    autocmd!
    autocmd WinLeave * set nocursorline nocursorcolumn
    autocmd WinEnter * set cursorline cursorcolumn
  augroup END

  " autocmd StdinReadPre * let s:std_in=1
  " autocmd VimEnter * if argc() == 0 && !exists('s:std_in') | NERDTree | endif

" }}}
