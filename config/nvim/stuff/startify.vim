let g:startify_session_dir = '~/.config/nvim/session'

let g:startify_lists = [
  \ { 'type': 'files',     'header': ['   Files']            },
  \ { 'type': 'dir',       'header': ['   Current Directory '. getcwd()] },
  \ { 'type': 'sessions',  'header': ['   Sessions']       },
  \ { 'type': 'bookmarks', 'header': ['   Bookmarks']      },
  \ ]

let g:startify_bookmarks = [
  \ { 'u': '~/Projekty/home/ubikcode/' },
  \ { 's': '~/Projekty/home/spacing-guild/' },
  \ { 'c': '~/.config/nvim/' },
  \ ]
 
let g:startify_custom_header = [
  \' ██╗   ██╗ ██████╗ ██╗   ██╗██╗███╗   ███╗',
  \' ██║   ██║██╔═══██╗██║   ██║██║████╗ ████║',
  \' ██║   ██║██║   ██║██║   ██║██║██╔████╔██║',
  \' ██║   ██║██║   ██║╚██╗ ██╔╝██║██║╚██╔╝██║',
  \' ╚██████╔╝╚██████╔╝ ╚████╔╝ ██║██║ ╚═╝ ██║',
  \'  ╚═════╝  ╚═════╝   ╚═══╝  ╚═╝╚═╝     ╚═╝',
  \]
