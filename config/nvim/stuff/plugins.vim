set rtp+=~/.config/nvim/bundle/Vundle.vim

call vundle#begin()

  Plugin 'VundleVim/Vundle.vim'
  Plugin 'dense-analysis/ale'
  Plugin 'preservim/nerdtree'
  Plugin 'Xuyuanp/nerdtree-git-plugin'
  Plugin 'morhetz/gruvbox'
  Plugin 'vim-airline/vim-airline'
  Plugin 'vim-airline/vim-airline-themes'
  Plugin 'terryma/vim-multiple-cursors'
  Plugin 'altercation/vim-colors-solarized'
  Plugin 'tpope/vim-fugitive'
  Plugin 'airblade/vim-gitgutter'
  Plugin 'zivyangll/git-blame.vim'
  Plugin 'preservim/nerdcommenter'
  " Plugin 'nathanaelkane/vim-indent-guides'
  Plugin 'tpope/vim-markdown' 
  Plugin 'sheerun/vim-polyglot'
  Plugin 'Shougo/deoplete.nvim'
  Plugin 'roxma/nvim-yarp'
  Plugin 'roxma/vim-hug-neovim-rpc'
  Plugin 'ctrlpvim/ctrlp.vim'
  Plugin 'qpkorr/vim-bufkill'
  Plugin 'mhinz/vim-startify'
  Plugin 'ryanoasis/vim-devicons'
  Plugin 'wfxr/minimap.vim'
  Plugin 'Xuyuanp/scrollbar.nvim'

call vundle#end()
