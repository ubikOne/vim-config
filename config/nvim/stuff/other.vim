" let g:indent_guides_enable_on_vim_startup = 1
" let g:indent_guides_guide_size = 1

let g:markdown_fenced_languages = ['html', 'python', 'bash=sh']
let g:markdown_syntax_conceal = 0
let g:markdown_minlines = 100

let g:deoplete#enable_at_startup = 1
