set nocompatible
filetype on
filetype plugin on
filetype indent on
filetype plugin indent on

syntax on

set cursorline
set cursorcolumn

set number relativenumber
set shiftwidth=2
set tabstop=2
set expandtab
set nobackup
set scrolloff=10
set incsearch
set ignorecase
set smartcase
set showcmd
set showmode
set showmatch
set hlsearch
set history=1000

set nu rnu
set autoindent
set smartindent

set wildmenu
set wildmode=list:longest
set wildignore=*.docx,*.jpg,*.png,*.gif,*.pdf,*.pyc,*.exe,*.flv,*.img,*.xlsx

set encoding=utf-8
set fileencoding=utf-8
set fileencodings=utf-8

cabbrev bterm bo term
cabbrev vterm vert term

set splitbelow
set splitright
