let mapleader = "\<space>"
nnoremap <leader>\ ``
nnoremap o o<esc>
nnoremap O O<esc>
nnoremap <Leader>tb :bo term<CR>
nnoremap <Leader>tv :vert term<CR>
nnoremap <Leader>z ZZ
nnoremap <Leader>q :q!<CR>
nnoremap <Leader>w :w<CR>
nnoremap <Leader><space> :

nnoremap <Leader>/ca zm
nnoremap <Leader>/co zc
nnoremap <Leader>/oa zr
nnoremap <Leader>/oo zo

nnoremap <C-t>     :tabnew<CR>
nnoremap <leader>tn :tabnew<CR>
inoremap <C-t>     <Esc>:tabnew<CR>
nnoremap <C-j> gT
nnoremap <C-k> gt

au TabLeave * let g:lasttab = tabpagenr()
nnoremap <silent> <c-l> :exe "tabn ".g:lasttab<cr>
vnoremap <silent> <c-l> :exe "tabn ".g:lasttab<cr>

map <C-o> :NERDTreeToggle<CR>
nnoremap <C-f> :NERDTreeFind<CR>

let g:multi_cursor_start_word_key      = '<C-n>'
let g:multi_cursor_select_all_word_key = '<A-n>'
let g:multi_cursor_start_key           = 'g<C-n>'
let g:multi_cursor_select_all_key      = 'g<A-n>'
let g:multi_cursor_next_key            = '<C-n>'
let g:multi_cursor_prev_key            = '<C-p>'
let g:multi_cursor_skip_key            = '<C-x>'
let g:multi_cursor_quit_key            = '<Esc>'

nmap ]h <Plug>(GitGutterNextHunk)
nmap [h <Plug>(GitGutterPrevHunk)

nnoremap <Leader>ga :Git add .<CR>
nnoremap <Leader>gc :Git commit<CR>
nnoremap <Leader>gsh :Git push<CR>
nnoremap <Leader>gll :Git pull<CR>
nnoremap <Leader>gs :Git status<CR>
nnoremap <Leader>gb :Git blame<CR>
nnoremap <Leader>gdd :Gvdiff<CR>
nnoremap <Leader>gd :Git diff<CR>
nnoremap <Leader>gr :Git remove<CR>
nnoremap <Leader>gss :<C-u>call gitblame#echo()<CR>

noremap <silent> <leader>c} V}:call NERDComment('x', 'toggle')<CR>
nnoremap <silent> <leader>c{ V{:call NERDComment('x', 'toggle')<CR>

nnoremap <leader>bn :bn<CR>
nnoremap <leader>bf :BF<CR> 
nnoremap <leader>bp :BB<CR> 
nnoremap <leader>bd :BD<CR> 
nnoremap <leader>bl :buffers!<CR>

nnoremap <leader>sn :ne<CR>

" splitting vim and moving around
nnoremap <leader>sh  :sp<CR>
nnoremap <leader>sv  :vsp<CR>
nnoremap <leader>sw <C-W><C-K>
nnoremap <leader>ss <C-W><C-J>
nnoremap <leader>sa <C-W><C-H>
nnoremap <leader>sd <C-W><C-L>

" session keebindings
nnoremap <leader>/ss :SSave
nnoremap <leader>/sl :SLoad<CR>
nnoremap <leader>/sd :SDelete[!]<CR>
nnoremap <leader>/sc :SClose<CR>

nnoremap <leader>/r  :registers 
