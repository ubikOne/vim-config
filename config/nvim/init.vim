"  ██╗   ██╗██╗███╗   ███╗                        
"  ██║   ██║██║████╗ ████║                        
"  ██║   ██║██║██╔████╔██║                        
"  ╚██╗ ██╔╝██║██║╚██╔╝██║                        
"   ╚████╔╝ ██║██║ ╚═╝ ██║                        
"    ╚═══╝  ╚═╝╚═╝     ╚═╝                        
"                                                 
"   ██████╗ ██████╗ ███╗   ██╗███████╗██╗ ██████╗ 
"  ██╔════╝██╔═══██╗████╗  ██║██╔════╝██║██╔════╝ 
"  ██║     ██║   ██║██╔██╗ ██║█████╗  ██║██║  ███╗
"  ██║     ██║   ██║██║╚██╗██║██╔══╝  ██║██║   ██║
"  ╚██████╗╚██████╔╝██║ ╚████║██║     ██║╚██████╔╝
"   ╚═════╝ ╚═════╝ ╚═╝  ╚═══╝╚═╝     ╚═╝ ╚═════╝ 
"                                                 
"  ███████╗██╗██╗     ███████╗                    
"  ██╔════╝██║██║     ██╔════╝                    
"  █████╗  ██║██║     █████╗                      
"  ██╔══╝  ██║██║     ██╔══╝                      
"  ██║     ██║███████╗███████╗                    
"  ╚═╝     ╚═╝╚══════╝╚══════╝                    
"                                                 


" VIM Config File
" Author: Bartek 'ubikOne' Błażejewski
" version 1.0.0
" License GNU GPL ver. 3

set runtimepath^=~/.vim runtimepath+=~/.vim/after
let &packpath = &runtimepath

set encoding=UTF-8

source ~/.config/nvim/stuff/base.vim
source ~/.config/nvim/stuff/plugins.vim
source ~/.config/nvim/stuff/startify.vim
source ~/.config/nvim/stuff/mapping.vim
source ~/.config/nvim/stuff/themes.vim
source ~/.config/nvim/stuff/git.vim
source ~/.config/nvim/stuff/scripts.vim
source ~/.config/nvim/stuff/other.vim
source ~/.config/nvim/stuff/minimap.vim

" NERDCOMMENTER ------------------------------------------------------- {{{

  let g:NERDCreateDefaultMappings = 1
  let g:NERDSpaceDelims = 1
  let g:NERDCompactSexyComs = 1
  let g:NERDDefaultAlign = 'left'
  let g:NERDAltDelims_java = 1
  let g:NERDCustomDelimiters = { 'c': { 'left': '/**','right': '*/' } }
  let g:NERDCommentEmptyLines = 1
  let g:NERDTrimTrailingWhitespace = 1
  let g:NERDToggleCheckAllLines = 1

" }}}

" CTRLP --------------------------------------------------------------- {{{

  set wildignore+=*/tmp/*,*.so,*.swp,*.zip     " MacOSX/Linux

  let g:ctrlp_custom_ignore = '\v[\/]\.(git|hg|svn)$'
  let g:ctrlp_custom_ignore = {
    \ 'dir':  '\v[\/]\.(git|hg|svn)$',
    \ 'file': '\v\.(exe|so|dll)$',
    \ 'link': 'some_bad_symbolic_links',
    \ }

" }}}
